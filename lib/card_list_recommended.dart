import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_rating/flutter_rating.dart';

class CardListRecommended extends StatefulWidget {
  @override
  _CardListRecommendedState createState() => _CardListRecommendedState();
}

class _CardListRecommendedState extends State<CardListRecommended> {
  PageController controller;
  int currentpage = 0;
  List<Widget> _cardViewRecommended = [];
  List<CardRecommended> cardRecommendedData = new List(3);

  @override
  initState() {
    super.initState();
    controller = PageController(
        initialPage: currentpage, keepPage: true, viewportFraction: 0.5);
  }

  @override
  dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    cardRecommendedData[0] = CardRecommended();
    cardRecommendedData[0].title = "Karpenisiou";
    cardRecommendedData[0].subtitleTitle = "Safosad, Oia, Greece";
    cardRecommendedData[0].score = 4.3;
    cardRecommendedData[0].temperatureInC = 34;
    cardRecommendedData[0].label = Icons.bookmark;
    cardRecommendedData[0].pathImageBackground =
        "https://www.discovergreece.com/~/media/images/article-background-images/santorini-the-one-and-only/santorini-view.ashx";
    cardRecommendedData[1] = CardRecommended();
    cardRecommendedData[1].title = "Neapoli Voion";
    cardRecommendedData[1].subtitleTitle = "Ofsasf, Perissa, Greece";
    cardRecommendedData[1].score = 4.6;
    cardRecommendedData[1].temperatureInC = 38;
    cardRecommendedData[1].label = Icons.bookmark_border;
    cardRecommendedData[1].pathImageBackground =
        "https://www.easyjet.com/en/holidays/shared/images/guides/greece/santorini.jpg";

    cardRecommendedData[2] = CardRecommended();
    cardRecommendedData[2].title = "Neapoli Voion";
    cardRecommendedData[2].subtitleTitle = "Ofsasf, Perissa, Greece";
    cardRecommendedData[2].score = 4.3;
    cardRecommendedData[2].temperatureInC = 26;
    cardRecommendedData[2].label = Icons.bookmark;
    cardRecommendedData[2].pathImageBackground =
        "https://i.pinimg.com/originals/68/b9/65/68b965da619ea94e5686b4cee04ce7af.jpg";

    return Scaffold(
      body: Container(
        height: 280,
        color: Colors.white,
        child: ListView.builder(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
            scrollDirection: Axis.horizontal,
            controller: controller,
            itemCount: cardRecommendedData.length,
            itemBuilder: (context, index) => _buildCardRecommended(index)),
      ),
    );
  }

  _buildCardRecommended(int index) {
    return Container(
      width: 145,
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.only(left: 24),
        child: Material(
            elevation: 8.0,
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
            shadowColor: Color.fromRGBO(253, 253, 253, 0.45),
            child: Column(
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Container(
                        height: 135,
                        width: double.infinity,
                        child: ClipRRect(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(8.0),
                              topRight: Radius.circular(8.0)),
                          child: Image.network(
                            cardRecommendedData[index].pathImageBackground,
                            fit: BoxFit.cover,
                          ),
                        )),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 16, horizontal: 10),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                            child: Text(
                              cardRecommendedData[index]
                                      .temperatureInC
                                      .round()
                                      .toString() +
                                  "°C",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 10),
                            ),
                          ),
                          Icon(
                            cardRecommendedData[index].label,
                            size: 14,
                            color: Colors.white,
                          )
                        ],
                      ),
                    )
                  ],
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(14.0),
                    child: Container(
                      width: double.infinity,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            cardRecommendedData[index].title,
                            style: TextStyle(
                                fontSize: 10, fontWeight: FontWeight.w600),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 6.0),
                            child: Text(
                              cardRecommendedData[index].subtitleTitle,
                              style: TextStyle(
                                  fontSize: 8,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.grey),
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: Row(
                                children: <Widget>[
                                  StarRating(
                                    size: 11,
                                    rating: cardRecommendedData[index].score,
                                    color: Color.fromRGBO(0, 118, 255, 1),
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 6.0),
                                      child: Text(
                                        cardRecommendedData[index].score.toString(),
                                        style: TextStyle(
                                            fontSize: 10,
                                            color: Color.fromRGBO(0, 118, 255, 1)),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            )),
      ),
    );
  }
}

class CardRecommended {
  String pathImageBackground;
  String title;
  String subtitleTitle;
  double score;
  double temperatureInC;
  IconData label;
}
