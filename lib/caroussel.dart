import 'package:flutter/material.dart';

class Carroussel extends StatefulWidget {
  @override
  _CarrousselState createState() => new _CarrousselState();
}

class _CarrousselState extends State<Carroussel> {
  PageController controller;
  int currentpage = 0;
  List<CarrousselData> carrousselDatas = new List(3);
  final double parallaxPercent = 0.0;
  List<Widget> _circleRow = [];

  @override
  initState() {
    super.initState();
    _buildACircleRow();
    controller = PageController(
        initialPage: currentpage, keepPage: false, viewportFraction: 0.85);
  }

  @override
  dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    carrousselDatas[0] = CarrousselData();
    carrousselDatas[0].pathImageBackground = "assets/maxresdefault.jpg";
    carrousselDatas[0].pathImageAvatar =
        "https://mobirise.com/bootstrap-template/profile-template/assets/images/timothy-paul-smith-256424-1200x800.jpg";
    carrousselDatas[0].nameAvatar = "Lelia Chavez";
    carrousselDatas[0].localizationAvatar = "Wallis and Futuna";
    carrousselDatas[0].score = "4.7";
    carrousselDatas[0].title = "Mediterranean";
    carrousselDatas[0].subtitle =
        "The Mediterranean Sea is a sea\nconnected to the Atlantic Ocean...";

    carrousselDatas[1] = CarrousselData();
    carrousselDatas[1].pathImageBackground = "assets/photo_sunset.jpg";
    carrousselDatas[1].pathImageAvatar =
        "https://mobirise.com/bootstrap-template/profile-template/assets/images/timothy-paul-smith-256424-1200x800.jpg";
    carrousselDatas[1].nameAvatar = "Lelia Chavez";
    carrousselDatas[1].localizationAvatar = "Wallis and Futuna";
    carrousselDatas[1].score = "4.9";
    carrousselDatas[1].title = "Mediterranean";
    carrousselDatas[1].subtitle =
        "The Mediterranean Sea is a sea\nconnected to the Atlantic Ocean...";

    carrousselDatas[2] = CarrousselData();
    carrousselDatas[2].pathImageBackground = "assets/photo_sunset_02.png";
    carrousselDatas[2].pathImageAvatar =
        "https://mobirise.com/bootstrap-template/profile-template/assets/images/timothy-paul-smith-256424-1200x800.jpg";
    carrousselDatas[2].nameAvatar = "Lelia Chavez";
    carrousselDatas[2].localizationAvatar = "Wallis and Futuna";
    carrousselDatas[2].score = "3.9";
    carrousselDatas[2].title = "Mediterranean";
    carrousselDatas[2].subtitle =
        "The Mediterranean Sea is a sea\nconnected to the Atlantic Ocean...";

    return new Scaffold(
      body: new Center(
        child: Column(
          children: <Widget>[
            Expanded(
              child: Container(
                color: Colors.white,
                child: PageView.builder(
                    onPageChanged: (value) {
                      setState(() {
                        currentpage = value;
                        _buildACircleRow();
                      });
                    },
                    controller: controller,
                    itemCount: 3,
                    itemBuilder: (context, index) => builder(index)),
              ),
            ),
            Container(
              height: 25,
              width: double.infinity,
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: _circleRow,
              ),
            )
          ],
        ),
      ),
    );
  }

  _buildACircleRow() {
    int i = 0;
    _circleRow.clear();

    carrousselDatas.forEach((element) {
      Color color = Color.fromRGBO(220, 224, 233, 1);
      Color colorCircle = Color.fromRGBO(220, 224, 233, 1);
      double heightCircle = 8;
      double widthCircle = 8;

      if (i == currentpage) {
        colorCircle = null;
        heightCircle = 10;
        widthCircle = 10;
        color = Color.fromRGBO(0, 118, 255, 1);
      }

      int page = i;
      _circleRow.add(GestureDetector(
        onTap: () {
          print(page);
          controller.animateToPage(page,
              duration: Duration(milliseconds: 500), curve: Curves.ease);
        },
        child: Padding(
          padding: EdgeInsets.all(6.0),
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(20)),
                border: Border.all(
                  color: color,
                  width: 2,
                  style: BorderStyle.solid
                ),
                color: colorCircle),
            height: heightCircle,
            width: widthCircle,
          ),
        ),
      ));
      i++;
    });
  }

  builder(int index) {
    return Padding(
      padding: EdgeInsets.all(10),
      child: Center(
        child: Container(
          height: 335.0,
          child: Stack(
            overflow: Overflow.clip,
            children: <Widget>[
              Container(
                height: 300,
                width: double.infinity,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: FractionalTranslation(
                    translation: Offset(parallaxPercent * 2.0, 0.0),
                    child: Image.asset(
                      carrousselDatas[index].pathImageBackground,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 16, horizontal: 24),
                  child: Container(
                    height: 50,
                    child: Row(
                      children: <Widget>[
                        CircleAvatar(
                          radius: 15,
                          backgroundColor: Colors.transparent,
                          backgroundImage: NetworkImage(
                              carrousselDatas[index].pathImageAvatar),
                        ),
                        Expanded(
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 16),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(carrousselDatas[index].nameAvatar,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12)),
                                  Text(
                                      carrousselDatas[index].localizationAvatar,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 10))
                                ],
                              ),
                            ),
                          ),
                        ),
                        Text(carrousselDatas[index].score,
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 22))
                      ],
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Material(
                  elevation: 16.0,
                  borderRadius: BorderRadius.circular(8.0),
                  shadowColor: Color.fromRGBO(253, 253, 253, 0.45),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: Container(
                      height: 105,
                      width: 270,
                      color: Colors.white,
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 24),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(carrousselDatas[index].title,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 18)),
                            Padding(
                              padding: EdgeInsets.only(top: 12.0),
                              child: Text(
                                carrousselDatas[index].subtitle,
                                style:
                                    TextStyle(fontSize: 12, color: Colors.grey),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class CarrousselData {
  String pathImageBackground;
  String pathImageAvatar;
  String nameAvatar;
  String localizationAvatar;
  String score;
  String title;
  String subtitle;
}
