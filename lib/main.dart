import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:travel_article_app/card_list_recommended.dart';
import 'package:travel_article_app/caroussel.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle.dark.copyWith(statusBarColor: Colors.transparent));
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.grey,
        ),
        home: MyHomePage());
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Container(
            child: Material(
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.only(top: 51.0, left: 38, right: 42),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Search for Place",
                              style: TextStyle(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.grey)),
                          Text("Destination",
                              style: TextStyle(
                                  fontSize: 28,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.black))
                        ],
                      ),
                    ),
                    CircleAvatar(
                        radius: 15,
                        backgroundColor: Colors.transparent,
                        backgroundImage: NetworkImage(
                          "https://mobirise.com/bootstrap-template/profile-template/assets/images/timothy-paul-smith-256424-1200x800.jpg",
                        )),
                  ],
                ),
              ),
            ),
          ),
          Container(height: 420, child: Carroussel()),
          Padding(
            padding: EdgeInsets.only(top: 32.0, left: 40.0, right: 40.0),
            child: Material(
              color: Colors.white,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      "Recommended",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                    ),
                  ),
                  Text(
                    "More",
                    style: TextStyle(color: Colors.grey),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: 290,
            child: CardListRecommended(),
          )
        ], // This trailing comma makes auto-formatting nicer for build methods.
      ),
    );
  }
}
